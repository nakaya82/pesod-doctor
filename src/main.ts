import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import './index.css'
import Toast, { PluginOptions, POSITION } from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";
const options:PluginOptions = {
  position: POSITION.BOTTOM_CENTER,
  timeout: 5000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: "button",
  transition: "Vue-Toastification__fade",
  icon: true,
  rtl: false
};
import PersonaInput from '@/components/PersonaInput.vue'
import PersonaCheckbox from '@/components/PersonaCheckbox.vue'
import PersonaSelect from '@/components/PersonaSelect.vue'

import BaseLayout from '@/layouts/BaseLayout.vue';
import FrameModal from '@/components/FrameModal.vue';

import ArrowRight from '@/components/ArrowRight.vue'
import UserIcon from '@/components/UserIcon.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheck, faCheckCircle, faTimes, faUserCheck } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { TRANSITION } from '@vue/compiler-dom'
library.add(faCheck, faCheckCircle, faUserCheck, faTimes)
const app = createApp(App)
app.use(router)
app.component('PInput', PersonaInput)
app.component('PCheckbox', PersonaCheckbox)
app.component('PSelect', PersonaSelect);
app.component('BaseLayout', BaseLayout)
app.component('FrameModal', FrameModal);

app.component('ArrowRight', ArrowRight);
app.component("UserIcon", UserIcon)
app.component('fa', FontAwesomeIcon )


app.use(Toast, options);
app.mount('#app');


