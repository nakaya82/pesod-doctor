import { AxiosError } from 'axios'
import { ref } from 'vue'
const apiError = ref('')
const screenHeight = ref(0);

export default function useAppState () {
  const stringifyApiError = (err: AxiosError) => {
    let str = '';
    const res = err.response;
    if (res) {
      const { status, data } = res;
      Object.entries(data).map(([k, v]: any, i: number) => {
        str += v;
        str += '<br>'
      })
      return str
    } else {
      return 'Unknown error'
    }
  }
  return {
    apiError,
    screenHeight,
    stringifyApiError
  }
}