import { ref, computed } from 'vue';
import { Product } from './Interfaces';
import useAxios from '@/types/Axios';


export default function useProducts() {
  const product = ref<Product | null>(null);
  const products = ref<Product[]>([]);
  const {
    client  
  } = useAxios();
  const fetchProducts = async (): Promise<Product[]> => {
    const { data } = await client.get('/products/');
    return data;
    
  };
  
  const getProduct = async (id: string) => {
    const {data} = await client.get(`/product/${id}`);
    return data;
  }
  
  return {
    product,
    products,
    fetchProducts,
    getProduct,
  }
}