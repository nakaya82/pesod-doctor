import { ref, computed } from 'vue';
import { Order, Subscription } from './Interfaces';
import useAxios from '@/types/Axios';

export default function useOrder() {
  const order = ref<Order | null>(null);
  const {client} = useAxios();

  const fetchUserOrders = async (uuid: string) => {
    const {data} = await client.get(`/orders/${uuid}`);
    console.log(data)
    return data;
  }
  
  return {
    order,
    fetchUserOrders
  }
}