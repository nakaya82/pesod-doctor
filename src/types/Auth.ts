import { Login, Doctor, AuthInfo, Registration } from '@/types/Interfaces';
import { ref, computed, ComputedRef, withDirectives, WritableComputedRef, Ref } from 'vue';
import axios from 'axios'

const AUTH_INFO_KEY = 'doctor_auth_info';
const API_BASE = `${import.meta.env.VITE_APP_HOST}/api`;
const authInfo = ref<AuthInfo | null>(null)

export default function useAuth () {
  const client = axios.create({
    baseURL: API_BASE
  });
  const accessToken = computed((): string | null => {
    if (authInfo.value == null) return null;
    return authInfo.value.access;
  })
  const refreshToken = computed((): string | null => {
    if (authInfo.value == null) return null;
    return authInfo.value.refresh;
  })
  const profile = computed((): Doctor | null => {
    if (authInfo.value == null) return null;
    return authInfo.value.user;
  })
  

  const createToken = async (postData: Login) => {
    const {data} = await client.post('/token/create/doctor', postData);
    console.log(data)
    return data;
    // window.localStorage.setItem('token', JSON.stringify(data));    
  };
  const storeAuthInfo = (data: AuthInfo) => {
    authInfo.value = data;
    window.localStorage.setItem(AUTH_INFO_KEY, JSON.stringify(data))
  }
  const refreshAccessToken = (accessToken: string) => {
    if (authInfo.value == null) return;
    const newAuthInfo = {
      ...authInfo.value,
      access: accessToken
    }
    storeAuthInfo(newAuthInfo);
  }
  
  const removeAuthInfo = () => {
    authInfo.value = null;
    window.localStorage.removeItem(AUTH_INFO_KEY);
  };
  
  
  
  const getAuthInfoFromLS = () => {
    const d = window.localStorage.getItem(AUTH_INFO_KEY)
    //alert(d)
    if (d == null) return null;
    return JSON.parse(d);
  }
  const sendResetPasswordLink = async (email: string) => {
    const {data} = await client.post('/password-reset/', { email: email })
    return data;
  }
  const resetPassword = async (uidb64: string, token: string, payload: object) => {
    const {data} = await client.post(`/password-reset/confirm/${uidb64}/${token}/`, payload)
    return data
  }
  
  
  return {
    createToken,
    storeAuthInfo,
    authInfo,
    profile,
    getAuthInfoFromLS,
    removeAuthInfo,
    accessToken,
    refreshToken,
    refreshAccessToken,
    resetPassword,
    sendResetPasswordLink
    
  }
}