interface Login{
  email: string;
  password: string;
}

interface Signup{
  email: string;
  password: string;
  password2: string;
}
interface Question{
  id: string;
  priority: number;
  title: string;
  question_body: string;
  question_types: string;
  keyName: string;
  qa_options: Option[];
  answer?: string;
}
interface Option{
 id: string;
 priority: number;
 option: string;
 help_text: string;
}

interface HairRecord{
  id: number;
  shooting_part: number;
  shooting_date: string;
  image: string;
}

interface Answer{
  id: string;
  customer: Customer;
  question: Question;
  prescript: string;
  descriptive_answer: string | null;
  answer_options: AnswerOption[];
  hair_record: HairRecord | null;
  hair_image?: string;
}
interface AnswerOption{
  id: number;
  answer: string;
  option: string;
}


interface CustomUser{
  email: string;
  uuid: string;
  id?: number;
  first_name: string;
  last_name: string;
  first_kana: string;
  last_kana: string;
  full_name: string;
  zip_code: string;
  prefecture: string;
  city: string;
  address: string;
  phone_number: string;
}


interface Customer extends CustomUser{
  icon_type: string;
  get_gender_display: string;
  has_subscribing: boolean;
  prescription_times: number;
  order_times: number;
}
interface Doctor extends CustomUser{
  
  clinic_name: string | null;
  description: string | null;
  examination_fee: number;
  image: string | null;
}
interface AuthInfo{
  access: string;
  refresh: string;
  user: Doctor;
}

interface Customer extends CustomUser{
  icon_type: string;
  birthday: string;
  age: number;
}
interface ChatLog{
  id: number;
  room_type: string;
  prescript: number;
  owner: number;
  speaker: number;
  message: string;
  created_at: string;
  situation?: string;
  online?: boolean;
}
interface MessageTemplate{
  id: number;
  template_type: string;
  message: string;
  subject: string;
  is_select: boolean;
}
interface ChatLogDto{
  uuid: string;
  message: string;
  situation: string;
}

interface Log{
  id: number;
  date: string;
  imgFront: string;
  imgTop: string;
  sunshine: boolean;
  alchol: boolean;
  cigar: boolean;
  exercise: boolean;
}
interface Prescript{
  id: string;
  customer: Customer;
  doctor: Doctor | null;
  prescript_no: string;
  prescript_date: string;
  prescription_use_period: null;
  prescript_products?: Product[];
  status: number;  
  connection?: WebSocket;
  unread_flag?: boolean;
  created_at: string;
}

interface Registration{
  first_name: string;
  last_name: string;
  first_kana: string;
  last_kana: string;
  phone_number: string;
  birthday: string;
  gender: number;
  prefecture: string;
  city: string;
  street: string;
  building: string;
  zip_code: string;
  password: string;
  address?: string;
}

interface Product{
  id: string;
  name: string;
  image: string;
  is_on_sale: boolean;
  categories: string;
  unit_price: number;
  maker: string;
  tax_rate: number;
  drug_class: number;
  quantity: number;
  usage: string;
  dose: string;
  using_period: string;
  is_doctor_recommend: boolean;
  is_sales: boolean;
}
interface Delivery{
  [key: string]: any;
  receiver: string;
  prefecture: string;
  city: string;
  street: string;
  building: string;
  address: string;
  zip_code: string;
  phone_number: string;
  deliv_time: number;
}
interface DeliveryValidator{
  [key: string]: any;
  receiver: Array<Function>;
  prefecture: Array<Function>;
  city: Array<Function>;
  street: Array<Function>;
  building: Array<Function>;
  address: Array<Function>;
  zip_code: Array<Function>;
  phone_number: Array<Function>;
  deliv_time: Array<Function>;  
}

interface SubscriptionProduct{
  product: Product;
  item_count: number;
  subtotal_with_tax: number;
}

interface Subscription{
  id: number;
  prescript: Prescript;
  policy_accepted?: boolean; 
  delivery_interval: number;
  deliv_time: number;
  get_deliv_req_time_display: string;
  zip_code: string;
  prefecture: string;
  city: string;
  address: string;
  phone_number: string;
  deliv_method: string;
  pay_method: number;
  get_pay_method_display: string;
  products: SubscriptionProduct[];
  next_delivery_date: string;
  subsc_status: number; 
  get_subsc_status_display: string;
  purchase_times: number;
  memo: string;
  created_at: string;
  charge: number;
  deliv_fee: number;
  relay_fee: number;
  total_amount_with_fee: number;
  total_amount: number;
}
interface OrderProduct{
  product: Product;
  item_count: number;
  subtotal_with_tax: number;
}
interface Order{
  id: string;
  order_status: number;
  get_order_status_display: string;
  customer: Customer;
  purchase_times: number;
  order_date: string;
  charge: number;
  deliv_fee: number;
  relay_fee: number;
  total_amount_with_fee: number;
  total_amount: number;
  products: OrderProduct[];
  deliv_req_time: number;
  get_deliv_req_time_display: string;
  subscription: Subscription;
  pay_method: number;
  get_pay_method_display: string;
  deliv_req_date: string;
  deliv_no: string;
  pay_status: number;
  get_pay_status_display: string;
  memo: string
}
interface Notification{
  level: number;
  message: string;
}
interface CustomerMemo{
  id: number;
  writer: CustomUser;
  customer: Customer;
  memo: string;
  created_at: string;
}
export {
  AuthInfo,
  Doctor,
  Login,
  Signup,
  Question,
  Option,
  Answer,
  AnswerOption,
  Customer,
  ChatLog,
  ChatLogDto,
  Log,
  Prescript,
  HairRecord,
  MessageTemplate,
  Registration,
  Product,
  Delivery,
  DeliveryValidator,
  Subscription,
  Order,
  OrderProduct,
  Notification,
  CustomerMemo 
}