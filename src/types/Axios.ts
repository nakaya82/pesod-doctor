import axios from 'axios';

// import { useRouter } from 'vue-router';

import router from '@/router';
import { useRoute } from 'vue-router';

import useAuth from './Auth'

const SERVER_NAME = import.meta.env.VITE_APP_HOST; //'http://localhost:8000';
const API_BASE = `${SERVER_NAME}/api`;

const isAuthUrl = (url: string | undefined) => {
  if (!url) return false;
  return url.includes('customer_registration') || url.includes('jwt') || url.includes('/token/') || url.includes('customer_pre_create') || url.includes('customer_activator') || url.includes('password');  
};

const isTokenRelatedError = (data: any) => {
  // alert(JSON.stringify(data))
  if (data == null) return false;
  if (data.message || data.detail) return true;
  return false;
}
export default function useAxios() {
  const route = useRoute();
  const {
    accessToken,
    refreshToken,
    storeAuthInfo,
    refreshAccessToken
  } = useAuth()
  
  const client = axios.create({
    baseURL: API_BASE
  });
  const getFreshAccessToken = async () => {
    try {
      const {data} = await client.post('/auth/jwt/refresh/', { refresh: refreshToken.value })
      return data;
    } catch (err) {
      console.error(err)
    }
    
  }
  client.interceptors.request.use(req => {
    if (isAuthUrl(req.url) || req.url === '/signup/') return req;
    
    if (accessToken.value == null) {
      router.push({ name: 'Login' });
      return req;
    }
    req.headers.Authorization = `JWT ${accessToken.value}`;
    return req;
  });
  
  client.interceptors.response.use(
    res => res,
    async error => {
      if (error.response == null) return;
      const { status, data } = error.response;
      
      if (!isAuthUrl(error.config.url) && status === 401 && isTokenRelatedError(data)) {
        if (error.config.__isRetry) {
          router.push({ name: 'Login', query: { next: route.path }});
        } else {
          const token = await getFreshAccessToken();
          if (token && token.access) {
            refreshAccessToken(token.access)
            error.config.__isRetry = true;
            // error.config.headers['Authorization']
            return await client(error.config)      
          }
        }
        throw error;
      }
      throw error;
    }
  );
  return {
    client
  }
}

export {
  SERVER_NAME
};