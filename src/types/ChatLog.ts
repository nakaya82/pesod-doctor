import { ref } from 'vue';
import { Customer, ChatLog, ChatLogDto } from './Interfaces';
import useAxios from './Axios';


export default function useChatLog() {
  const {
    client
  } = useAxios();

  const chatLogs = ref<ChatLog[]>([]);
  const chatLogPage = ref(1);
  const fetchDoctorChatLogs = async (prescriptId: string): Promise<ChatLog[]> => {
    const {data} = await client.get(`/chatlog/doctor/${prescriptId}`)
    return data;  
  };
  
  const fetchCustomerMessageTemplates = async () => {
    const {data} = await client.get(`/customer_tpl_msg/`);
    console.log(data)
    return data
  }
  const fetchDoctorMessageTemplates = async () => {
    const {data} = await client.get(`/doctor_tpl_msg/`);
    console.log(data)
    return data
  }
  const updateChatLogCursor = async (owner: number, prescriptId: string, cursor: number) => {
    const payload = {
      room_type: 'doctor',
      owner: owner,
      prescript: prescriptId,
      cursor: cursor
    }
    const {data} = await client.patch('/chatcursor/', payload)
    console.log(data)
    return data
  }
  const getChatCursor = async (prescriptNo: string) => {
    const {data} = await client.get(`/chatcursor/doctor/${prescriptNo}`);
    console.log(data)

    return data
  }
  return {
    chatLogs,
    fetchDoctorChatLogs,
    fetchDoctorMessageTemplates,
    fetchCustomerMessageTemplates,
    updateChatLogCursor,
    getChatCursor
  }
}