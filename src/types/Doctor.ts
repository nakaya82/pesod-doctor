import { ref, computed } from 'vue';
import { Doctor } from './Interfaces';
import useAxios from './Axios';




// const myDoctor = computed(() => {
//   return doctors.value.find(d => d.id===1);
// })
export default function useDoctors() {
  const {
    client
  } = useAxios()

  const doctors = ref<Doctor[]>([]);
  const doctor = ref<Doctor | null>(null);
  
  const fetchDoctors = async (): Promise<Doctor[]> => {
    const { data } = await client.get('/doctors/');
    return data
  }
  
  const getDoctor = async (id: number): Promise<Doctor> => {
    const { data } = await client.get(`/doctor/${id}`);
    return data;
  }
  return {
    // myDoctor,
    doctor,
    doctors,
    getDoctor,
    fetchDoctors
  }
}