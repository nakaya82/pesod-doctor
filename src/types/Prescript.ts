

import { ComputedRef, computed, ref, WritableComputedRef } from 'vue';
import { Prescript, Product } from '@/types/Interfaces';
import useAxios from '@/types/Axios';

const PRESCRIPT_STATUS_STARTED = 1
const PRESCRIPT_STATUS_QUES_DONE = 2
const PRESCRIPT_STATUS_SELECT_DOCTOR = 3
const PRESCRIPT_STATUS_ACCEPT_INFO = 4
const PRESCRIPT_STATUS_PRESCRIBED = 5
const PRESCRIPT_STATUS_EXAM_DONE = 6
const PRESCRIPT_STATUS_RESULT_CONFIRMED = 7
const PRESCRIPT_STATUS_DONE = 0

export default function usePrescript () {
  const {
    client
  } = useAxios();
  const myPrescript = ref<Prescript | null>(null);
  const prescripts = ref<Prescript[]>([]);

  const getPrescriptById = async (uuid: string): Promise<Prescript> => {
    const {data} = await client.get(`/prescript/${uuid}`)
    return data;
  }
  const fetchPrescripts = async (): Promise<Prescript[]> => {
    const {data} = await client.get('/prescripts/')
    return data;
  }
  const fetchUserPrescripts = async (uuid: string) => {
    const {data} = await client.get(`/prescripts/${uuid}`);
    return data
  }

  const setPrescriptProducts = async (prescriptNo: string, products: Product[]) => {
    const prescriptProducts = products.map((p: Product) => ({ product: p.id }));
    const {data} = await client.patch(`/prescribe/${prescriptNo}`, { products: prescriptProducts });
    return data;
  }

  const acceptPrescriptProducts = async () => {
    const {data} = await client.patch('/prescript/exam_fix', {});
    return data;
  };

  const deletePrescriptProducts = async (prescriptNo: string) => {
    const {data} = await client.delete(`/prescribe/${prescriptNo}`);
    return data;

  }


  return {
    // prescript,
    PRESCRIPT_STATUS_STARTED,
    PRESCRIPT_STATUS_QUES_DONE,
    PRESCRIPT_STATUS_SELECT_DOCTOR,
    PRESCRIPT_STATUS_ACCEPT_INFO,
    PRESCRIPT_STATUS_PRESCRIBED,
    PRESCRIPT_STATUS_EXAM_DONE,
    PRESCRIPT_STATUS_RESULT_CONFIRMED,
    PRESCRIPT_STATUS_DONE,
    myPrescript,
    prescripts,
    getPrescriptById,
    fetchPrescripts,
    fetchUserPrescripts,
    setPrescriptProducts,
    acceptPrescriptProducts,
    deletePrescriptProducts
    
  }
}