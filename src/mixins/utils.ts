import moment from 'moment'

const getFormattedDate = (str: string, format: string) => {
  return moment(str).isValid() ? moment(str).format(format) : '-'  
}
const htmlify = (s: string) => {
  return s.replaceAll('\n', '<br />')
}
export {
  htmlify,
  getFormattedDate, 
}