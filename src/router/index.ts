import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('@/views/Dashboard.vue'),
  },
  {
    path: '/password-reset',
    name: 'PasswordReset',
    component: () => import('@/views/PasswordReset.vue'),
    children: [
      {
        path: 'sendlink',
        name: 'PasswordResetSendLink',
        component: () => import('@/views/PasswordReset/SendLink.vue')
      },
      {
        path: 'sendlink/sent',
        name: 'PasswordResetLinkSent',
        component: () => import('@/views/PasswordReset/LinkSent.vue')
      },
      {
        path: 'setpassword/:uidb64/:token',
        name: 'PasswordResetSetPassword',
        component: () => import('@/views/PasswordReset/SetPassword.vue')
      },
      {
        path: 'setpassword/done',
        name: 'PasswordResetDone',
        component: () => import('@/views/PasswordReset/Done.vue')
      },
      
      
      
    ]
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { left: 0, top: 0 }
  }
})

export default router
